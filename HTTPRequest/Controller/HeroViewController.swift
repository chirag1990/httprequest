//
//  HeroViewController.swift
//  HTTPRequest
//
//  Created by Chirag Tailor on 18/01/2018.
//  Copyright © 2018 Chirag Tailor. All rights reserved.
//

import UIKit

class HeroViewController: UIViewController {
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var attributeLbl: UILabel!
    @IBOutlet weak var attackLbl: UILabel!
    @IBOutlet weak var legsLbl: UILabel!
    
    var hero:HeroStats?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    func setupUI() {
        guard let heroStat = hero else {return}
        
        
        nameLbl.text = heroStat.localized_name
        attributeLbl.text = heroStat.primary_attr
        attackLbl.text = heroStat.attack_type
        legsLbl.text = "\((heroStat.legs))"
        
        let imgURL = getImageUrl(url: heroStat.img)
        guard let imageURL = URL(string: imgURL) else {return}
        imageView.downloadedFrom(url: imageURL)
        
    }
}

extension UIImageView {
    func downloadedFrom(url: URL, contentMode mode: UIViewContentMode = .scaleAspectFit) {
        contentMode = mode
        URLSession.shared.dataTask(with: url) { data, response, error in
            guard
                let httpURLResponse = response as? HTTPURLResponse, httpURLResponse.statusCode == 200,
                let mimeType = response?.mimeType, mimeType.hasPrefix("image"),
                let data = data, error == nil,
                let image = UIImage(data: data)
                else { return }
            DispatchQueue.main.async() {
                self.image = image
            }
            }.resume()
    }
    func downloadedFrom(link: String, contentMode mode: UIViewContentMode = .scaleAspectFit) {
        guard let url = URL(string: link) else { return }
        downloadedFrom(url: url, contentMode: mode)
    }
}
