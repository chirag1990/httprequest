//
//  Constants.swift
//  HTTPRequest
//
//  Created by Chirag Tailor on 18/01/2018.
//  Copyright © 2018 Chirag Tailor. All rights reserved.
//

import Foundation

typealias CompletionHandler = (_ Success: Bool) -> ()

let BASE_URL = "https://api.opendota.com"
let GET_HEROS_URL = "\(BASE_URL)/api/heroStats"

func getImageUrl(url : String) -> String {
    return "\(BASE_URL)\(url)"
}

