//
//  HeroService.swift
//  HTTPRequest
//
//  Created by Chirag Tailor on 18/01/2018.
//  Copyright © 2018 Chirag Tailor. All rights reserved.
//

import Foundation

class HeroService {
    static let instance = HeroService()
    
    var dataSource : [HeroStats] {
        get {
            return heroes
        }
    }
    
    fileprivate var heroes = [HeroStats]()
    
    func getHero(completion: @escaping CompletionHandler) {
        
        guard let url = URL(string: GET_HEROS_URL) else {return}
        
        URLSession.shared.dataTask(with : url) {(data, response, err) in
            
            guard let data = data else {return}
            
            if err == nil {
                do {
                    self.heroes = try JSONDecoder().decode([HeroStats].self, from: data)
                    completion(true)
                }catch {
                    print ("Error Parsing JSON")
                    completion(false)
                }
            }
            
        }.resume()
        
    }
}
