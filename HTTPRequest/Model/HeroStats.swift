//
//  HeroStats.swift
//  HTTPRequest
//
//  Created by Chirag Tailor on 18/01/2018.
//  Copyright © 2018 Chirag Tailor. All rights reserved.
//

import Foundation

struct HeroStats : Decodable {
    
    let localized_name : String
    let primary_attr : String
    let attack_type : String
    let legs : Int
    let img : String
}
